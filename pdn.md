<center><h1>Prise de note</center></h1>


# Dans cmd :

* mkdir NomDossier : Permet de créer un dossier.
* rmdir NomDossier : Permet de supprimer un dossier.
* dir : Permet de lister les fichiers/dossiers.présent dans le dossier.
_____________________
# Ultilisation git ligne de commande :

* Git clone ULR : Permet de cloner le git en local.
* Git status : Permet de savoir le status du dossier GIT, voir les fichiers non ajoutés.
* Git add NomDuFichier : Permet d'ajouter un fichier à envoyer sur Git.
* Git commit -m "Info de la maj fichier" : Permet de commit et d'informer sur la derniere update
* Git pull : Permet de récupérer les derniers commits du Git.
* Git push : Permet d'envoyer les derniers commits sur Git.
_____________________
# Python:

* For NomBoucle range(NombreExecution) :
    print("Message")
    Permet de repeter une instruction un certain nombre de fois (NombreExecution). Ne pas oublier de mettre des espaces pour indiquer qu'elle « appartient » à l'instruction précédente.
__________________________
# Php

* Changer le type de retour d'une fonction :
    * <php>function test(int $varint) : String<br/>
        {<br/>
            $varint = 10;<br/>
            return $varint;<br/>
        }<br/>
        echo test(5); // Affiche la string "10"<br/>
        </php>
* Stocker une fonction dans une variable :
    * <php>function test(int $varint)<br/>
        {<br/>
            echo $varint;<br/>
        }<br/>
        $afficherVarint = "test";<br/>
        $afficherVarint(5); // Affiche l'entier 5
        </php>

    * <php>
        $Bonjour = function()<br/>
        {<br/>
            echo "Bonjour !";<br/>
        };<br/>
        $Bonjour(); // Affiche la string "Bonjour !"

* var_dump($variable); // Permet d'afficher toutes les infos d'une variable
* afficher l'indice d'un tableau dans une boucle foreach :
    * <php>
        $tab = [1,2,3];<br/>
        foreach ($tab as $value => $indice)<br/>
        {<br/>
            echo $value; // Permet d'afficher la valeur actuelle (via son indice).<br/>
            echo $indice; // Permet d'afficher l'indice de la valeur actuelle.<br/>
        }<br/>
